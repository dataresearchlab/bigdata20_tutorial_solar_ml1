#### [IEEE BigData 2020](http://bigdataieee.org/BigData2020/) / [Tutorials](http://bigdataieee.org/BigData2020/Tutorials.html) /  [Tutorial 6](http://bigdataieee.org/BigData2020/Tutorials.html#tutorial6)
## Data Sources, Tools, and Techniques for Big Data-driven Machine Learning in Heliophysics

* **Tutorial Presenters:** [*Azim Ahmadzadeh*](https://www.azim-a.com/), [*Dustin J. Kempton*](https://grid.cs.gsu.edu/~dkempton1/), [*Berkay Aydin*](https://grid.cs.gsu.edu/~baydin2/), and [*Rafal A. Angryk*](https://grid.cs.gsu.edu/~rangryk/)
* **Affiliation:** [Georgia State University](https://www.gsu.edu/) - [Computer Science Department](https://www.cs.gsu.edu/)
* **Demo Author:** [*Azim Ahmadzadeh*](https://www.azim-a.com/) (`aahmadzadeh1@cs.gsu.edu`)
* **Last Modified:** Dec 3, 2020

**Tutorial Abstract.**

During the past decade, Georgia State University’s (GSU) Data Mining
Lab (DMLab) has been conducting research on a wide range of topics
centering on understanding, detection, and forecast of solar events,
those of which can (directly or indirectly) have significant 
economic and collateral impacts on mankind, through electromagnetic
radiation and energetic particles. The close collaboration of the
Computer Scientists and Solar Physicists with the sole dedication
to research on solar events using advanced statistical tools,
machine learning (ML) and deep learning (DL), resulted in a couple
of hundreds of in-depth studies in this domain. Many of these
studies have been published in prestigious journals such as
Nature’s Scientific Data and The Astrophysical Journal. We would
like to prepare a tutorial on some methodologies we
engineered, the challenges we faced, and the products we put
together. We believe our solutions and products can stimulate new
data-driven discoveries in heliophysics, as well as to serve and
inspire communities of other domains.

----
## Tutorial Materials

The slides and the video of the 2-hour tutorial can be found below:

* **Slides:**
https://dmlab.cs.gsu.edu/bigdata/bigdata-tutorial-2020/BigData20_Tutorial_6_slides.pdf
  
* **Video:**
https://dmlab.cs.gsu.edu/bigdata/bigdata-tutorial-2020/BigData2020_Tutorial_backupTalks2.mp4

----

## Demo A
This project contains the first of the two demos we presented in
this tutorial. It gives a live demo to showcase how the [Ten Image
Parameter dataset](https://iopscience.iop.org/article/10.3847/1538-4365/ab253a),
and the built [web API](http://dmlab.cs.gsu.edu/dmlabapi/isd_docs.html)
can be used.

For Demo B, please visit [bitbucket.org/azimdmlab/bigdata20_tutorial_solar_ml2/](https://bitbucket.org/azimdmlab/bigdata20_tutorial_solar_ml2/src/master/).

----
#### Requirements
*  Python 3.8
*  For a list of all required packages, see [requirements.txt](./requirements.txt).

----
#### Run/View it online
Click on the badges below to try the demo ([./_Walk_through/ImageParamTutorial.ipynb](./_Walk_through/ImageParamTutorial.ipynb)) online.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fazimdmlab%2Fbigdata20_tutorial_solar_ml1/master)
[![nbviewer](https://img.shields.io/badge/render-nbviewer-orange.svg)](https://nbviewer.jupyter.org/urls/bitbucket.org/azimdmlab/bigdata20_tutorial_solar_ml1/raw/913573836a91c24630ae3c02effec82e7069e4cf/_Walk_through/ImageParamTutorial.ipynb)


**Note**: The rich html/markdown cells of the demo are best rendered in [JupyterLab](https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html#:~:text=JupyterLab%20is%20a%20next%2Dgeneration,%2C%20integrated%2C%20and%20extensible%20manner.)
([Jupyter](https://jupyter.org/) ’s next-generation notebook interface). Switching to JupyterLab
environment is as simple as replacing the `/tree` (at the end of the url)
with `/lab`. This should be done after you clicked on the 'binder' badge above,
and the build is complete.